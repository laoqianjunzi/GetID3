GetId3

Useful links
------------
* [Main site](http://www.getid3.org)
* [Support](http://support.getid3.org)

License
-------

For license info please read [Resources/doc/license.txt](https://github.com/phansys/GetId3/tree/master/Resources/doc/license.txt)

For commercial license read [Resources/doc/license.commercial.txt](https://github.com/phansys/GetId3/tree/master/Resources/doc/license.commercial.txt)

## Installation via [composer](http://getcomposer.org/)

##### Run composer to install the library:

``` bash
$ composer require "laoqianjunzi/getid3: ~2.1"
```

Quick use example reading audio properties
------------------------------------------
``` php
<?php
namespace My\Project;

use GetId3\GetId3Core;

class MyClass
{
    // ...
    private function myMethod()
    {
        $mp3File = '/path/to/my/mp3file.mp3';
        $getId3 = new GetId3Core();
        $audio = $getId3
            ->setOptionMD5Data(true)
            ->setOptionMD5DataSource(true)
            ->setEncoding('UTF-8')
            ->analyze($mp3File)
        ;

        if (isset($audio['error'])) {
            throw new \RuntimeException(sprintf('Error at reading audio properties from "%s" with GetId3: %s.', $mp3File, $audio['error']));
        }
        $this->setLength(isset($audio['playtime_seconds']) ? $audio['playtime_seconds'] : '');

        // var_dump($audio);
    }
}

```
